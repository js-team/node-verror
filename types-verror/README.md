# Installation
> `npm install --save @types/verror`

# Summary
This package contains type definitions for verror (https://github.com/davepacheco/node-verror).

# Details
Files were exported from https://github.com/DefinitelyTyped/DefinitelyTyped/tree/master/types/verror.

### Additional Details
 * Last updated: Fri, 02 Jul 2021 18:05:01 GMT
 * Dependencies: none
 * Global values: none

# Credits
These definitions were written by [Sven Reglitzki](https://github.com/svi3c), and [Maxime Toumi-M](https://github.com/max4t).
